<?php
/**
 * Author: Nick Venenga
 * Description: This provides an API-like interface for the employee portal at 88sears
 */
 
 class EmployeeData {
     private $enterpriseId; //enterprise id
     private $password;
     private $cookie; //need to store cookies from web pages
     private $x; //the value of the x variable on 88sears pages
     protected $employeeIdNumber;
	 protected $checksList = array();
     
     function __construct($eid, $password) {
         $this->enterpriseId = $eid;
         $this->password = $password;
         $cname = "ccookie-".uniqid().".txt";
         $this->cookie = tempnam(sys_get_temp_dir(), "$cname");
         $this->login();
     }
     
     protected function curl($uri, $data = null) { //gets a page via curl and returns the results
        $handle = curl_init();
        
        curl_setopt($handle, CURLOPT_URL, $uri);
        if(isset($data)) {
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        else {
            curl_setopt($handle, CURLOPT_POST, false);
        }
        curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($handle, CURLOPT_COOKIEFILE, $this->cookie);
        curl_setopt($handle, CURLOPT_COOKIEJAR, $this->cookie);
        curl_setopt($handle, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64");
        curl_setopt($handle, CURLOPT_PROXY, "http://127.0.0.1:8888");
    
        $response = curl_exec($handle);
        $hlength  = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);
        $body     = substr($response, $hlength);
    
        // If HTTP response is not 200, throw exception
        if ($httpCode != 200) {
            //throw new Exception($httpCode);
        }
        return $body;
     }
     
     private function login() { //logs in and gets the value of x
         $loginUrl = "https://hr.searshc.com/aos/ldapLogon.do";
         $postFields = array(
                "act" => "logon",
                "enterpriseID" => $this->enterpriseId,
                "password" => $this->password
         );
         $loggedInPage = $this->curl($loginUrl, $postFields);
         $this->x = $this->findX($loggedInPage);
     }
     
     private function findX($page) { //finds the value of x given the html on a page (x is a token needed to load pages)
		 if(strpos($page, 'id="x"') !== false) {//html page
			$regex = "/(?<=id=\"x\" value=\").*?[\n]{1}.*?(?=\"\/>)/";
			preg_match($regex, $page, $x);
			return $x[0];
		 }
		 else if(strpos($page, '"x":"')) {//json page
			preg_match("/\{.*\}/s", $page, $json);
			$json = json_decode($json[0]);
			return $json->x;
		 }
     }
     
     protected function getPage($name, $data) { //gets a page on 88sears and updates x
		$url = "https://hr.searshc.com/aos/";
		$pageContents = $this->curl($url . $name, $data);
		$this->x = $this->findX($pageContents);
		return $pageContents;
     }
     
     public function getEmployeeIdNumber() { //retrieves the 12 digit number used to clock in
         if(isset($this->employeeIdNumber)) {
            return $this->employeeIdNumber;
         }
         else {
			$pageContents = $this->getPage("servlet/json", array("ajaxCmd" => "AH038", "x" => $this->x)); 
			preg_match("/\{.*\}/s", $pageContents, $json); //this page has a strange header that curl doesn't remove, this gets the pure json
			$json = json_decode($json[0]);
			$this->employeeIdNumber = $json->emplid;
			return $this->employeeIdNumber;
         }
     }
     
     public function paySummary() { //retrieves pay summary information available on "My Pay" page
		$pageContent = $this->getPage("mypay.do", array("act" => "Read", "x" => $this->x));
		preg_match("/(?<=<table class=\"outline noborder\" style=\"border:1px solid black;\">).*(?=<!--=== Employee Information)/s", $pageContent, $_earnings); //get earnings table
		preg_match_all("/(?<=<tr>).*?(?=<\/tr>)/s", $_earnings[0], $tableRows); //get each table row
		$paySummary = array();
		array_shift($tableRows[0]); //first element is headers and not needed
		foreach($tableRows[0] as $index => $cell) { //loop through table cells in each row
			preg_match_all("/(?<=>).*(?=<)/", $cell, $data); //get contents of table cell
			$data = $data[0];
			if($index <=3 ) { //first chunk is earnings/deductions with current and year to date
				$paySummary["Earnings"][$data[0]]["Current Period"] = $data[1];
				$paySummary["Earnings"][$data[0]]["Y-T-D"] = $data[2];
			}
			else {//second chunk is vacation with 1 column
				$paySummary["Vacation"][$data[0]] = $data[1];
			}
		}
		return $paySummary;
     }
     
     public function listChecks() { //gets a list of check dates and amounts
		if(count($this->checksList) < 1) {
			$pageContent = $this->getPage("mypay.do", array("act" => "Read", "x" => $this->x));
			preg_match_all("/<option.*?<\/option>/s", $pageContent, $_checks);
			$checks = array();
			foreach($_checks[0] as $check) {
				preg_match("/(?<=\").*(?=\")/", $check, $value); //get the value in between the quotation marks (check id)
				$id = $value[0];
				preg_match("/(?<=>).*(?=<)/", $check, $dateAmount);
				$dateAmount = explode(' ... ', $dateAmount[0]);
				$date = $dateAmount[0];
				$amount = $dateAmount[1];
				$this->checksList[] = array("id" => $id, "date" => $date, "amount" => $amount);
			}
		}
		return $this->checksList;
     }
     
     public function getCheck($id) { //gets a specific check and the information
        $pageContents = $this->getPage("mypay.do", array("act" => "detail", "check_ID" => $id, "x" => $this->x));
		preg_match_all("/(?<=<table class=\"outline noborder\" style=\"border:1px solid black;\">).*?(?=<\/table>)/s", $pageContents, $tables);
		$tables = $tables[0];
		$check = array();
		//Overview
		preg_match_all("/(?<=>).*(?=<\/td>)/", $tables[0], $_overview);
		for($i = 0; $i < count($_overview[0]); $i++) {
			if(strpos($_overview[0][$i], 'deposited') === false) { //deposited means this cell is a message about what account the money was deposited into
				$check['Overview'][$_overview[0][$i]] = $_overview[0][$i+1];
				$i++; 
			} else {
				$check['Overview']['Deposits'][] = $_overview[0][$i]; //it's possible to have more than one if pay is setup to deposit into multiple accounts
			}
		}
		//Hours and Earnings
		preg_match_all("/(?<=>).*(?=<\/td>)/", $tables[1], $_earnings); //get all the cell's contents
		$_earnings = array_slice($_earnings[0], 7); //remove unwanted title and column names
		for($i = 0; $i < count($_earnings); $i += 6) {
			$check['Hours and Earnings'][$_earnings[$i]] = array(
					"Rate" => $_earnings[$i+1],
					"Current Hours" => $_earnings[$i+2],
					"Current Amount" => $_earnings[$i+3],
					"Y-T-D Hours" => $_earnings[$i+4],
					"Y-T-D Amount" => $_earnings[$i+5]
			);
		}
		//Taxes (Before and After Deductions are excluded since I don't have these setup and wasn't sure how the data was displayed)
		preg_match("/.*(?=<!--== Before Tax Deduction ===========================================  -->)/s", $tables[2], $_taxes);
		preg_match_all("/(?<=>).*(?=<\/td>)/", $_taxes[0], $taxes); //get all the cell's contents
		$taxes = array_splice($taxes[0], 4); //get rid of column headings
		for($i = 0; $i < count($taxes); $i += 3) {
			$append = '';
			while(isset($check['Taxes'][$taxes[$i].$append])) { //I have two tax categories with the same name. This presents the previous from getting overwritten by the latter by appending 0s to the end of the name until it is unique
				$append .= '0';
			}
			$check['Taxes'][$taxes[$i].$append] = array(
					"Current Amount" => $taxes[$i+1],
					"Y-T-D Amount" => $taxes[$i+2]
			);
		}
		return $check;
     }
     
	 public function getCheckIdByDate($date) {//gets a check id given a date in the format MM/DD/YYYY
	 }
	 
     public function getSchedule() { //gets an array of all the scheduled hours
         
     }
     
     public function getHours($date) { //gets the scheduled hours for a certain day
         
     }
 }
